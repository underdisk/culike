################################################################################
#                                                                              #
#     This code is licensed under the GNU GPLv3 or later                       #
#     More information in the LICENSE file that should have                    #
#     been given with the source code                                          #
#                                                                              #
################################################################################

#!/bin/bash
if ! [ -f "vars.sh" ]; then
	printf "'%s' not found. Do ./setup.sh to create it with a visual tool\n" vars.sh
	exit
fi

source vars.sh

KERNEL_NAME="culike"
KERNEL_VERSION="21.1.2"

mkdir -p build
mkdir -p sysroot
mkdir -p sysroot/usr
mkdir -p sysroot/usr/include
mkdir -p sysroot/usr/lib

SYSROOT_INCLUDE_DIR="sysroot/usr/include"
SYSROOT_LIB_DIR="sysroot/usr/lib"

if [ "$arch" == "x86" ]; then
	_arch="i386"
	BITNESS_FLAGS=-m32
else
	_arch=$arch
fi

LIBC_DIR="src/libc"
LIBC_INCLUDE_DIR="$LIBC_DIR/include"
LIBC_C_FLAGS="-std=c99 $BITNESS_FLAGS -ffreestanding -mno-red-zone -mno-mmx -mno-sse -mno-sse2 -O3 -fPIC"

# BUILDING LIBC
make -j -C $LIBC_DIR CC="$cc" AR="$ar" CFLAGS="$LIBC_C_FLAGS"

# MOVING LIBC FILES INTO SYSROOT
cp -r $LIBC_INCLUDE_DIR/** $SYSROOT_INCLUDE_DIR
cp $LIBC_DIR/libc.a $SYSROOT_LIB_DIR


GENERIC_INCLUDE_DIR="src/kernel/generic/include"
PLATFORM_INCLUDE_DIR="src/kernel/$_arch/$plat/include"
KERNEL_C_FLAGS="-std=c99 -ffreestanding -mno-red-zone -mno-mmx -mno-sse -mno-sse2 -O3 -fPIC -DKERNEL_NAME='\"$KERNEL_NAME\"' -DKERNEL_VERSION='\"$KERNEL_VERSION\"' -DKERNEL_INFO"

make -j -C src/kernel/$_arch CC="$cc" AR="$ar" LD="$ld" AS="$as" CFLAGS="$KERNEL_C_FLAGS" INC="$(pwd)/$SYSROOT_INCLUDE_DIR $(pwd)/$PLATFORM_INCLUDE_DIR $(pwd)/$GENERIC_INCLUDE_DIR" LIB="$(pwd)/$SYSROOT_LIB_DIR"
cp	src/kernel/$_arch/$plat/kernel.elf build/kernel.elf
