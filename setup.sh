################################################################################
#                                                                              #
#     This code is licensed under the GNU GPLv3 or later                       #
#     More information in the LICENSE file that should have                    #
#     been given with the source code                                          #
#                                                                              #
################################################################################

#!/bin/bash
TERM=ansi

if ! command -v whiptail &> /dev/null; then
	printf "The setup script requires the 'whiptail' program\n\
You can find this tool on your favorite package manager \
(including homebrew on macOS). the package is likely called 'newt'\n"
exit
fi

function menu() { whiptail "$@" 3>&1 1>&2 2>&3 3>&- ; }

if [ -f "vars.sh" ]; then
	source vars.sh
else
	menu --clear --title "Kernel Build Setup" --msgbox "You will be guided throught the kernel build setup" 8 50
fi


CULIKE_BUILD_ARCH=$(menu --title "Kernel Build Setup" --menu "What architecture do you target?" 8 50 0 \
"x86" "")
if [ $CULIKE_BUILD_ARCH == "x86" ]; then
	CULIKE_BUILD_PLAT="pc"
#else
	#$(menu --title "Kernel Build Setup" --menu "What platform do you target?" 8 50 0 "pc" "")
fi
CULIKE_BUILD_CC=$(menu --title "Kernel Build Setup" --inputbox "Command to invoke your C Cross Compiler" 8 50 "$cc")
CULIKE_BUILD_LD=$(menu --title "Kernel Build Setup" --inputbox "Command to invoke your Cross Linker" 8 50 "$ld")
CULIKE_BUILD_AR=$(menu --title "Kernel Build Setup" --inputbox "Command to invoke your Cross AR (.o archiver)" 8 50 "$ar")
CULIKE_BUILD_AS=$(menu --title "Kernel Build Setup" --inputbox "Command to invoke your assembler (note: nasm on x86)" 8 50 "$as")

echo arch=\"$CULIKE_BUILD_ARCH\" > vars.sh
echo plat=\"$CULIKE_BUILD_PLAT\" >> vars.sh
echo cc=\"$CULIKE_BUILD_CC\" >> vars.sh
echo ar=\"$CULIKE_BUILD_AR\" >> vars.sh
echo ld=\"$CULIKE_BUILD_LD\" >> vars.sh
echo as=\"$CULIKE_BUILD_AS\" >> vars.sh

printf "\n\n\nvars.sh created successfuly. do ./build.sh to build\n"
