################################################################################
#                                                                              #
#     This code is licensed under the GNU GPLv3 or later                       #
#     More information in the LICENSE file that should have                    #
#     been given with the source code                                          #
#                                                                              #
################################################################################

#!/bin/bash

mkdir -p iso
mkdir -p iso/boot
mkdir -p iso/boot/grub
cp build/kernel.elf iso/boot/
echo 'set timeout=1' > iso/boot/grub/grub.cfg
echo 'set default=0' >> iso/boot/grub/grub.cfg
echo '' >> iso/boot/grub/grub.cfg
echo 'menuentry "Multiboot2" {' >> iso/boot/grub/grub.cfg
echo '	multiboot2 /boot/kernel.elf' >> iso/boot/grub/grub.cfg
echo '	boot' >> iso/boot/grub/grub.cfg
echo '}' >> iso/boot/grub/grub.cfg
grub-mkrescue -v -o kernel.iso iso/
