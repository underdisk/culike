# Cute Little Kernel

This is a small x86 32-bit kernel i'm doing as a side project for fun.

For now it supports:

- Multiboot 1 and 2 specification for booting
- VGA output
- Freestanding Standard C library (using a modified [FsLibc](https://github.com/Velko/FsLibc))
- Exceptions
- Interrupts

## Building prerequisites

You will need:

* A UNIX(-like) system (Only tested on macOS 10.14)

* An i386-elf (or x86_64-elf) cross toolchain containing:

  * A C compiler
  * A Linker (LD)
  * An object archiver (AR)

  Clang (LLVM) works for compiling (precise --target=i386-elf), but you will probably have to build GNU Binutils to have a i386-elf linker and archiver

* The Netwide assembler (NASM)

* GRUB2 (or any Multiboot 1/2 compliant bootloader)

* Xorriso (used by GRUB to build the iso file)

* whiptail (for the setup script, install 'newt' in your favorite package manager)

## Building

### Setup stage

Run `./setup.sh`

You will then enter a configuration process where you can precise your toolchain.

Note: if you wish to use clang for cross-compiling, you can put the target flag next to the command directly in the setup script `clang --target=i386-elf`

### Build stage

Run `./build.sh`

If no errors occur, then the kernel is built. If you wish to run it you likely want an iso image

### Create the ISO file

Run `./iso.sh`

The iso file should be present on the root directory and be called `kernel.iso`

### Testing the kernel

If the right flavor of QEMU is installed (`qemu-system-i386` or `qemu-system-x86_64`), you can directly run the iso file by doing `./qemu <ARCH>`

### Cleaning the project

If you wish to clean the project files simply run `./clean.sh`
