/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#include "dsctbl/gdt.h"
#include <string.h>

extern void	set_gdt(struct gdt_entry *, u32);
extern struct gdt_ptr gdtr;
extern void reload_segments();

struct gdt_entry_smpl	GDT_tmp[16];
struct gdt_entry		GDT[16];
void					*GDT_end;

void	encode_gdt_entry(u8 *target, struct gdt_entry_smpl *source)
{
	if ((source->limit > 65536) && (source->limit & 0xFFF) != 0xFFF) {
		return ; //todo: panic
	}
	if (source->limit > 65536) {
		source->limit >>= 12;
		target[6] = 0xC0;
	} else {
		target[6] = 0x40;
	}

	target[0] = source->limit & 0xFF;
	target[1] = (source->limit >> 8) & 0xFF;
	target[6] |= (source->limit >> 16) & 0xF;

	target[2] = source->base & 0xFF;
	target[3] = (source->base >> 8) & 0xFF;
	target[4] = (source->base >> 16) & 0xFF;
	target[7] = (source->base >> 24) & 0xFF;

	target[5] = source->type;
}

struct gdt_ptr	gdt_init(void)
{
	GDT_tmp[0] = (struct gdt_entry_smpl) {	.base=0, .limit=0, .type=0	};
	GDT_tmp[1] = (struct gdt_entry_smpl) {	.base=0, .limit=0xFFFFFFFF, .type=0x9A	};	/* .text */
	GDT_tmp[2] = (struct gdt_entry_smpl) {	.base=0, .limit=0xFFFFFFFF, .type=0x92	};	/* .data */
	GDT_tmp[3] = (struct gdt_entry_smpl) {	.base=0, .limit=0, .type=0x89	};			/* future tss segment */

	for (u32 i = 0; i < 4; ++i) {
		encode_gdt_entry((u8*)&GDT[i], &GDT_tmp[i]);
	}

	set_gdt(GDT, sizeof(GDT));
	reload_segments();

	return (gdtr);
}
