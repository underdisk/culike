;##############################################################################;
;                                                                              ;
;     This code is licensed under the GNU GPLv3 or later                       ;
;     More information in the LICENSE file that should have                    ;
;     been given with the source code                                          ;
;                                                                              ;
;##############################################################################;

extern GDT
extern GDT_end

global gdtr
global set_gdt
global reload_segments

gdtr:				dw 		0
					dd 		0
set_gdt:			mov		eax, [esp + 4]
					mov		[gdtr + 2], eax
					mov		ax, [esp + 8]
					mov		[gdtr], ax
					cli
					lgdt	[gdtr]
					sti
					ret
reload_segments:	jmp		0x08:.reload_cs
.reload_cs:			mov		ax, 0x10
					mov		ds, ax
					mov		es, ax
					mov		fs, ax
					mov		gs, ax
					mov		ss, ax
					ret
