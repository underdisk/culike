;##############################################################################;
;                                                                              ;
;     This code is licensed under the GNU GPLv3 or later                       ;
;     More information in the LICENSE file that should have                    ;
;     been given with the source code                                          ;
;                                                                              ;
;##############################################################################;

global		idt_load;
extern		idt_ptr;

idt_load:	lidt	[idt_ptr]
			sti
			ret
