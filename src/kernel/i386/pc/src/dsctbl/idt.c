/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#include <dsctbl/idt.h>
#include <string.h>

struct idt_entry	idt[256];
struct idt_ptr		idt_ptr;

extern void	idt_load();

void		idt_add_entry(u8 num, u32 base, u16 sel, u8 flags)
{
    idt[num].zero = 0;
    idt[num].base_lo = (u16)(base & 0xFFFF);
    idt[num].base_hi = (u16)(base >> 16) & 0xFFFF;
    idt[num].sel = sel;
    idt[num].flags = flags;
}

void		idt_install()
{
	idt_ptr.limit = (sizeof(struct idt_entry) * 256) - 1;
	memset((u8*)&idt, 0, sizeof(idt));
	idt_ptr.base = (u32)&idt;
	idt_load();
}
