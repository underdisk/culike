/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#include <driver/keyboard.h>
#include <types.h>
#include <port.h>

#include <interrupts/isr.h>
#include <stdio.h>

struct keyboard_cmd_queue kcq;

void	keyboard_callback()
{
	u8	scancode = inb(KEYBOARD_ENCODER);
	keyboard_queue_add((struct keyboard_cmd) {.command = scancode});
}

void	keyboard_queue_init()
{
	kcq.front = -1;
	kcq.rear = -1;
}

bool	keyboard_queue_is_empty()
{
	return	(kcq.front == kcq.rear);
}

void	keyboard_queue_add(struct keyboard_cmd cmd)
{
	if (keyboard_queue_is_empty()) {
		kcq.front = 0;
	}
	kcq.rear++;
	kcq.rear %= 256;
	kcq.data[kcq.rear] = cmd;
}

struct keyboard_cmd	keyboard_queue_del()
{
	struct keyboard_cmd	ret;

	if (keyboard_queue_is_empty()) {
		return (struct keyboard_cmd) {0, 0};
	}
	ret = kcq.data[kcq.front];
	kcq.front++;
	kcq.front %= 256;
	return (ret);
}

char	keyboard_gc()
{
	volatile i16 is_empty;
	while (1) {
		is_empty = keyboard_queue_is_empty();
		printf("");
		if (!is_empty) {
			break ;
		}
	}
	struct keyboard_cmd cmd = keyboard_queue_del();
	printf("%p\n", cmd.command);
}

void	setup_keyboard()
{
	keyboard_queue_init();
	set_interrupt_handler(33, (isr_t)keyboard_callback);
}
