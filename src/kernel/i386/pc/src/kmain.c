/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#if defined(__linux__) || defined(__APPLE__)
#error "You should use a cross compiler"
#endif

#include <string.h>

#include <kernel.h>

#include <interrupts/pic.h>
#include <interrupts/isr.h>

#include <port.h>
#include <vga/vga.h>

#include <memhelper.h>
#include <driver/keyboard.h>

static void	put_ok()
{
	vga_text_setcolor(COLOR_WHITE);
	vga_text_putc('[');
	vga_text_setcolor(COLOR_GREEN);
	vga_text_puts("OK");
	vga_text_setcolor(COLOR_WHITE);
	vga_text_putc(']');
}

static void	put_fail()
{
	vga_text_setcolor(COLOR_WHITE);
	vga_text_putc('[');
	vga_text_setcolor(COLOR_RED);
	vga_text_puts("FAIL");
	vga_text_setcolor(COLOR_WHITE);
	vga_text_putc(']');
}

/*
 *	Startup string
 */
void	startup()
{
	vga_init();

	gdt_init();
	put_ok();
	printf(" Installed GDT\n");

	put_ok();
	pic_remap(0x20, 0x28);
	printf(" Remapped PIC\n");

	idt_install();
	put_ok();
	printf(" Installed IDT\n");

	isr_install();
	put_ok();
	printf(" Installed ISR\n");
	
	setup_keyboard();
	put_ok();
	printf(" Setup Keyboard\n");
}

/*
 *	Kernel entry point
 */
EXPORT_FUNCTION void	kmain(void)
{
	startup();
	vga_text_setcolor(COLOR_YELLOW);
	printf("\n%s v%s", KERNEL_NAME, KERNEL_VERSION);
	vga_text_setcolor(COLOR_WHITE);
	printf(" (unstable)\n\n");

	while (1)
		keyboard_gc();

	for(;;) {
    	__asm("hlt");
 	}
}
