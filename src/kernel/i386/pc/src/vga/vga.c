/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#include "vga/vga.h"
#include <port.h>

unsigned char *const __vga_mem__	= (unsigned char *const)VGA_VIDEO_PTR;
struct __vga_buffer_s __vga_buffer__;

static int vga_text_putc_file(int c, FILE * f)
{
	(void)f;
	vga_text_putc(c);
	return (0);
}

/*
 * Inits the VGA struct
 */
void	vga_init()
{
	vga_enable_cursor(0, 15);
	__vga_buffer__.color = COLOR_WHITE;
	__vga_buffer__.x = 0;
	__vga_buffer__.y = 0;

	__vga_buffer__.scr_term.putc = vga_text_putc_file;
    __vga_buffer__.scr_term.getc = NULL; // todo
    __vga_buffer__.scr_term.pre_output = NULL; // todo
    __vga_buffer__.scr_term.post_output = NULL; // todo
    __vga_buffer__.scr_term.ungetc_buf = -1;
    __vga_buffer__.scr_term.user_ptr = NULL;
    stdin = stdout = &__vga_buffer__.scr_term;
}

/*
 * Disables the cursor
 */
void	vga_disable_cursor()
{
	outb(0x3D4, 0x0A);
	outb(0x3D5, 0x20);
}

/*
 * Sets the text cursor position
 */
void vga_set_cursor_pos(u16 x, u16 y)
{
	u16 pos = y * VGA_HORIZONTAL_LIMIT + x;
 
	outb(0x3D4, 0x0F);
	outb(0x3D5, (u8) (pos & 0xFF));
	outb(0x3D4, 0x0E);
	outb(0x3D5, (u8) ((pos >> 8) & 0xFF));
}

/*
 * Sets the cursor position
 */
void	vga_enable_cursor(u16 x, u16 y)
{
	outb(0x3D4, 0x0A);
	outb(0x3D5, (inb(0x3D5) & 0xC0) | x);
 
	outb(0x3D4, 0x0B);
	outb(0x3D5, (inb(0x3D5) & 0xE0) | y);
}

/*
 * Clears the entire VGA text buffer (at 0xB8000)
 */
void	vga_text_clear()
{
	__vga_buffer__.x = 0;
	__vga_buffer__.y = 0;
	u16 i = 0;
	while(i < VGA_HORIZONTAL_LIMIT * VGA_VERTICAL_LIMIT * 2) {
		__vga_mem__[i++] = 0;
		__vga_mem__[i++] = COLOR_WHITE;
	}
	vga_set_cursor_pos(0, 0);
}

/*
 * Puts a character at the end of the text buffer stream
 * Wraps line at newline characters (\n, 0xA) or at the end of the VGA screen
 */
void	vga_text_putc(unsigned char c)
{
	u16 offset = VGA_HORIZONTAL_LIMIT * __vga_buffer__.y
			+ __vga_buffer__.x;
	if (c == '\n') {
		__vga_buffer__.x = VGA_HORIZONTAL_LIMIT;
	} else {
		unsigned char *mem = __vga_mem__ + (offset * 2);
		*mem++ = c;
		*mem++ = __vga_buffer__.color;
	}
	if (++__vga_buffer__.x >= VGA_HORIZONTAL_LIMIT) {
		__vga_buffer__.x = 0;
		++__vga_buffer__.y;
	}
	if (__vga_buffer__.y >= VGA_VERTICAL_LIMIT) {
		vga_text_clear();
	}
	vga_set_cursor_pos(__vga_buffer__.x, __vga_buffer__.y);
}

/*
 * Puts a string at the end of the text buffer stream
 * Wraps line at newline characters (\n, 0xA) or at the end of the VGA screen
 */
void	vga_text_puts(const char *s)
{
	while (*s) {
		vga_text_putc(*s++);
	}
}

/*
 * Sets the VGA color used for text display
 * Will display text with this color until it is changed
 */
void	vga_text_setcolor(VGAColor color)
{
	__vga_buffer__.color = color;
}
