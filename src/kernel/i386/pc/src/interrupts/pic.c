/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#include <types.h>
#include <port.h>
#include <interrupts/pic.h>

void pic_remap(u32 offset1, u32 offset2)
{
	u32 a1, a2;

	a1 = inb(PIC1_DATA);
	a2 = inb(PIC2_DATA);

	outb(PIC1_COMMAND, PIC_INIT);	// restart PIC1
	outb(PIC2_COMMAND, PIC_INIT);	// restart PIC2

	outb(PIC1_DATA, offset1);		// remap PIC1
	outb(PIC2_DATA, offset2);		// remap PIC2

	outb(PIC1_DATA, 0x04);			// setup cascading
	outb(PIC2_DATA, 0x02);

	outb(PIC1_DATA, 0x01);			// 8086
	outb(PIC2_DATA, 0x01);

	outb(PIC1_DATA, a1);			// restore masks
	outb(PIC2_DATA, a2);
}
