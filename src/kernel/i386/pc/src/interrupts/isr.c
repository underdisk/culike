/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#include "interrupts/isr.h"
#include <stdio.h>
#include <port.h>
#include <vga/vga.h>

isr_t	int_handlers[256];

void	set_interrupt_handler(u8 n, isr_t handler)
{
	int_handlers[n] = handler;
}

const char*	except_messages[] = {
    "Divide By Zero exception",
    "Debug exception",
    "non maskable interrupt exception",
    "Breakpoint exception",
    "Into Detected Overflow exception",
    "Out Of bounds exception",
    "Invalide Opcode exception",
    "No Coprocessor exception",
    "Double Fault exception",
    "Coprocessor Segment Overrun Exception",
    "Bad TSS exception",
    "Segment Not Present Exception",
    "Stack Fault Exception",
    "General Protection Fault Exception",
    "Page Fault Exception",
    "Unknown Interrupt Exception",
    "Coprocessor Fault Exception",
    "Alignment Check Exception",
    "Machine Check Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception",
    "Reserved Exception"
};

void	isr_install()
{
	idt_add_entry(0, (u32)isr0, 0x08, 0x8E);
    idt_add_entry(1, (u32)isr1, 0x08, 0x8E);
    idt_add_entry(2, (u32)isr2, 0x08, 0x8E);
    idt_add_entry(3, (u32)isr3, 0x08, 0x8E);
    idt_add_entry(4, (u32)isr4, 0x08, 0x8E);
    idt_add_entry(5, (u32)isr5, 0x08, 0x8E);
    idt_add_entry(6, (u32)isr6, 0x08, 0x8E);
    idt_add_entry(7, (u32)isr7, 0x08, 0x8E);
    idt_add_entry(8, (u32)isr8, 0x08, 0x8E);
    idt_add_entry(9, (u32)isr9, 0x08, 0x8E);
    idt_add_entry(10, (u32)isr10, 0x08, 0x8E);
    idt_add_entry(11, (u32)isr11, 0x08, 0x8E);
    idt_add_entry(12, (u32)isr12, 0x08, 0x8E);
    idt_add_entry(13, (u32)isr13, 0x08, 0x8E);
    idt_add_entry(14, (u32)isr14, 0x08, 0x8E);
    idt_add_entry(15, (u32)isr15, 0x08, 0x8E);
    idt_add_entry(16, (u32)isr16, 0x08, 0x8E);
    idt_add_entry(17, (u32)isr17, 0x08, 0x8E);
    idt_add_entry(18, (u32)isr18, 0x08, 0x8E);
    idt_add_entry(19, (u32)isr19, 0x08, 0x8E);
    idt_add_entry(20, (u32)isr20, 0x08, 0x8E);
    idt_add_entry(21, (u32)isr21, 0x08, 0x8E);
    idt_add_entry(22, (u32)isr22, 0x08, 0x8E);
    idt_add_entry(23, (u32)isr23, 0x08, 0x8E);
    idt_add_entry(24, (u32)isr24, 0x08, 0x8E);
    idt_add_entry(25, (u32)isr25, 0x08, 0x8E);
    idt_add_entry(26, (u32)isr26, 0x08, 0x8E);
    idt_add_entry(27, (u32)isr27, 0x08, 0x8E);
    idt_add_entry(28, (u32)isr28, 0x08, 0x8E);
    idt_add_entry(29, (u32)isr29, 0x08, 0x8E);
    idt_add_entry(30, (u32)isr30, 0x08, 0x8E);
    idt_add_entry(31, (u32)isr31, 0x08, 0x8E);

	idt_add_entry(32, (u32)irq0, 0x08, 0x8E); //timer
    idt_add_entry(33, (u32)irq1, 0x08, 0x8E); //keyboard

    idt_add_entry(35, (u32)irq3, 0x08, 0x8E); // COM 2
    idt_add_entry(36, (u32)irq4, 0x08, 0x8E); // COM 1
    idt_add_entry(37, (u32)irq5, 0x08, 0x8E); // LPT2
    idt_add_entry(38, (u32)irq6, 0x08, 0x8E); // Floppy disk

    idt_add_entry(39, (u32)irq7, 0x08, 0x8E); // LPT1/Spurious 
    idt_add_entry(40, (u32)irq8, 0x08, 0x8E); // CMOS real-time
    idt_add_entry(41, (u32)irq9, 0x08, 0x8E); // SCSI/NIC
    idt_add_entry(42, (u32)irq10, 0x08, 0x8E); // SCSI/NIC
    idt_add_entry(43, (u32)irq11, 0x08, 0x8E); // SCSI/NIC
    idt_add_entry(44, (u32)irq12, 0x08, 0x8E); // Mouse
    idt_add_entry(45, (u32)irq13, 0x08, 0x8E); // FPU
    idt_add_entry(46, (u32)irq14, 0x08, 0x8E); // Primary ATA
    idt_add_entry(47, (u32)irq15, 0x08, 0x8E); // Secondary ATA

	outb(0x21, 0xFD);
	outb(0xA1, 0xFF);
	__asm("sti");
}

static void	debug_regs(const char* as, u32 a, const char* bs, u32 b, const char* cs, u32 c)
{
	vga_text_setcolor(COLOR_YELLOW);
	printf("%s", as);
	vga_text_setcolor(COLOR_WHITE);
	printf(": %p ", a);
	vga_text_setcolor(COLOR_YELLOW);
	printf("%s", bs);
	vga_text_setcolor(COLOR_WHITE);
	printf(": %p ", b);
	vga_text_setcolor(COLOR_YELLOW);
	printf("%s", cs);
	vga_text_setcolor(COLOR_WHITE);
	printf(": %p\n", c);
}

void	fault_handler(struct regs *r)
{
	vga_text_setcolor(COLOR_PURPLE);
	printf("*** - !!! KERNEL PANIC !!!! - ***\n");
	vga_text_setcolor(COLOR_LIGHT_RED);
	printf("A kernel-level exception was raised!\n");
	printf("Exception details:\n");
	if (r->int_no < 32) {
		printf("  - %s\n", except_messages[r->int_no]);
		printf("  - Error code: %d\n", r->err_code);
		printf("  - Exception number: %d\n", r->int_no);
	} else {
		printf("Unknown Exception. What did you even do ?\n");
	}
	vga_text_setcolor(COLOR_WHITE);
	printf("Registers:\n");
	debug_regs("  eip", r->eip, "eax", r->eax, "ebx", r->ebx);
	debug_regs("  ecx", r->ecx, "edp", r->edx, "esp", r->esp);
	debug_regs("  ebp", r->ebp, "esi", r->esi, "edi", r->edi);
	printf("Due to the critical nature of this exception, the system will halt.\n");
	while (1);
}

void	irq_handler(struct regs *r)
{
	if (r->int_no >= 40) {
		outb(0xA0, 0x20);
	}
	outb(0x20, 0x20);

	isr_t handler = int_handlers[r->int_no];
	if (handler) {
		handler(*r);
	}
}
