;##############################################################################;
;                                                                              ;
;     This code is licensed under the GNU GPLv3 or later                       ;
;     More information in the LICENSE file that should have                    ;
;     been given with the source code                                          ;
;                                                                              ;
;##############################################################################;

; Loader assembly code for Multiboot-complient bootloaders ;

				global start				; ELF entry point

				section .multiboot
MB_MAGIC		equ	0x1BADB002				; used by Multiboot-complient
											; bootloaders to locate the
											; multiboot header
MB_FLAGS		equ	0x0						; multiboot flags
MB_CHECKSUM		equ	-(MB_MAGIC + MB_FLAGS)	; CHECKSUM + FLAGS + MAGIC_NUMBER
											; should
											; equal zero

MB2_MAGIC		equ	0xE85250D6							; used by Multiboot2-complient
														; bootloaders to locate the
														; multiboot header
MB2_ARCH		equ	0x0									; 0x0 means i386
MB2_LEN			equ	0xc									; multiboot2 header length
MB2_CHECKSUM	equ	-(MB2_MAGIC + MB2_ARCH + MB2_LEN)	; CHECKSUM + ARCH +FLAGS + MAGIC_NUMBER
														;should
														; equal zero

				section .text
align 4
				dd		MB_MAGIC
				dd		MB_FLAGS
				dd		MB_CHECKSUM

align 8
				dd		MB2_MAGIC
				dd		MB2_ARCH
				dd		MB2_LEN
				dd		MB2_CHECKSUM

				dw		0x0								; end of MB2 tags: type 0
				dw		0x0
				dd		0x8								; end of MB2 tags: size 8
extern kmain
start:			mov		esp, stack_top
				call	kmain
				cli
.halt:			hlt
				jmp .halt

				section .bss
stack_bottom:	resb	4096 * 8
stack_top:
