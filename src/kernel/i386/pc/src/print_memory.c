/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_memory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hroussea <hroussea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 12:14:18 by hroussea          #+#    #+#             */
/*   Updated: 2021/01/13 12:42:46 by hroussea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <vga/vga.h>

static void	ft_putchar(unsigned char c)
{
	vga_text_putc(c);
}

static void	dot_or_char(unsigned char *str, int size)
{
	while (size-- && str++)
		ft_putchar(*(str - 1) >= 32 && *(str - 1) <= 126 ? *(str - 1) : '.');
}

static void	put_hex(unsigned char c)
{
	ft_putchar((c > 9) ? 'a' + c - 10 : '0' + c);
}

static int		put_line(void *addr, int *size)
{
	int	i;
	int	j;

	i = -1;
	while (++i < 8)
		put_hex(((unsigned long)addr >> 4 * (16 - i - 1)) % 16);
	ft_putchar(':');
	i = -1;
	while (++i < 16)
	{
		if (i % 2 == 0)
			ft_putchar(' ');
		if ((*size)-- > 0)
		{
			j = i + 1;
			put_hex(((unsigned char*)addr)[i] / 16);
			put_hex(((unsigned char*)addr)[i] % 16);
		}
		else
			dot_or_char((unsigned char*)"  ", 2);
	}
	return (j);
}

void	*ft_print_memory(void *addr, int size)
{
	int len;
	while (size > 0)
	{
		len = put_line(addr, &size);
		ft_putchar(' ');
		dot_or_char((unsigned char*)addr, len);
		ft_putchar('\n');
		addr += 0x10;
	}
	return (addr);
}
