/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#pragma once

#include <types.h>

static inline void	outb(u16 port, u8 val)
{
	__asm volatile ("outb	%0, %1" : : "a"(val), "Nd"(port));
}

static inline u8 inb(u16 port)
{
	u8	ret;
	__asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"(port));
	return (ret);
}
