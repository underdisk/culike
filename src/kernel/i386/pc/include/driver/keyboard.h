/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#pragma once

#include <types.h>

#define	KEYBOARD_ENCODER 	0x60
#define	KEYBOARD_CONTROLLER	0x64

struct	keyboard_cmd {
	u8	command;
	u8	response;
};

struct	keyboard_cmd_queue {
	struct keyboard_cmd	data[256];
	i16					rear;
	i16					front;
};

char	keyboard_gc();

void	keyboard_queue_init();

bool	keyboard_queue_is_empty();

void	keyboard_queue_add(struct keyboard_cmd cmd);

struct keyboard_cmd	keyboard_queue_del();

void	setup_keyboard();
