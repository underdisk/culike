/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#pragma once

#include <types.h>

#define	GDT_ENTRY_TEXT 1
#define	GDT_ENTRY_RODATA 2
#define	GDT_ENTRY_DATA 3

/*
 * Description of a single GDT entry, packed to avoir
 * compiler optimization
 */
struct	gdt_entry
{
	u16	limit_lo;
	u16	base_lo;
	u8	base_middle;
	u8	access;
	u8	limit_hi:4;
	u8	other:4;
	u8	base_hi;
}	__attribute__((packed));

struct	gdt_entry_smpl
{
	u32		base;
	u32		limit;
	u8		type;
}	__attribute__((packed));

/*
 * base: pointer to the base of the GDT
 * limit: size of the GDT minus 1
 */
struct	gdt_ptr
{
	u16	limit;
	u32	base;
}	__attribute__((packed));

/* Places an entry in the GDT */
void			gdt_set(u32 base, u32 limit, u8 access, u8 gran, struct gdt_entry *desc);

/* Initializes the GDT */
struct gdt_ptr	gdt_init();
