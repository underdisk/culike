/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#pragma once

#include <types.h>

struct	idt_entry {
	u16	base_lo;
	u16	sel;
	u8	zero;
	u8	flags;
	u16	base_hi;
} __attribute__((__packed__));

struct	idt_ptr
{
	u16	limit;
	u32	base;
} __attribute__((__packed__));

void	idt_add_entry(u8 num, u32 base, u16 sel, u8 flags);
void	idt_install();
