/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#pragma once

#include <types.h>
#include <export.h>
#include <dsctbl/gdt.h>
#include <dsctbl/idt.h>

#define BOCHS_BREAKPOINT() __asm("xchg bx, bx");

#ifndef KERNEL_INFO
	#define KERNEL_NAME "KERNEL_NAME_NOT_DEFINED"
	#define KERNEL_VERSION "KERNEL_VERSION_NOT_DEFINED"
#endif
