/* ************************************************************************** */
/*                                                                            */
/*     This code is licensed under the GNU GPLv3 or later                     */
/*     More information in the LICENSE file that should have                  */
/*     been given with the source code                                        */
/*                                                                            */
/* ************************************************************************** */

#pragma once

#include <types.h>
#include <stdio.h>

#define VGA_VIDEO_PTR			0xB8000
#define VGA_HORIZONTAL_LIMIT	80
#define VGA_VERTICAL_LIMIT		25

typedef enum	VGAColor {
	COLOR_BLACK			= 0x0,
	COLOR_BLUE			= 0x1,
	COLOR_GREEN			= 0x2,
	COLOR_CYAN			= 0x3,
	COLOR_RED			= 0x4,
	COLOR_PURPLE		= 0x5,
	COLOR_BROWN			= 0x6,
	COLOR_GRAY			= 0x7,
	COLOR_DARK_GRAY		= 0x8,
	COLOR_LIGHT_BLUE	= 0x9,
	COLOR_LIGHT_GREEN	= 0xA,
	COLOR_LIGHT_CYAN	= 0xB,
	COLOR_LIGHT_RED		= 0xC,
	COLOR_LIGHT_PURPLE	= 0xD,
	COLOR_YELLOW		= 0xE,
	COLOR_WHITE			= 0xF,

} 				VGAColor;

struct __vga_buffer_s {
	VGAColor		color;
	u8				x, y;
	FILE			scr_term;
};

/*
 * Inits the VGA struct
 */
void	vga_init();

/*
 * Disables the cursor
 */
void	vga_disable_cursor();

/*
 * Sets the text cursor position
 */
void vga_set_cursor_pos(u16 x, u16 y);

/*
 * Sets the cursor position
 */
void	vga_enable_cursor(u16 x, u16 y);

/*
 * Clears the entire VGA text buffer (at 0xB8000)
 */
void	vga_text_clear();

/*
 * Puts a character at the end of the text buffer stream
 * Wraps line at newline characters (\n, 0xA) or at the end of the VGA screen
 */
void	vga_text_putc(unsigned char c);

/*
 * Puts a string at the end of the text buffer stream
 * Wraps line at newline characters (\n, 0xA) or at the end of the VGA screen
 */
void	vga_text_puts(const char *s);

/*
 * Sets the VGA color used for text display
 * Will display text with this color until it is changed
 */
void	vga_text_setcolor(VGAColor color);
