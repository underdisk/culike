################################################################################
#                                                                              #
#     This code is licensed under the GNU GPLv3 or later                       #
#     More information in the LICENSE file that should have                    #
#     been given with the source code                                          #
#                                                                              #
################################################################################

#!/bin/bash
qemu-system-$1 -cdrom kernel.iso
