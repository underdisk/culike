################################################################################
#                                                                              #
#     This code is licensed under the GNU GPLv3 or later                       #
#     More information in the LICENSE file that should have                    #
#     been given with the source code                                          #
#                                                                              #
################################################################################

#!/bin/bash
if [ -f "vars.sh" ]; then
	source vars.sh
else
	echo No vars.sh found. Cannot clear
	exit
fi

if [ "$arch" == "x86" ]; then
	arch="i386"
fi

make -C src/libc/ clean
make -C src/kernel/$arch/$plat clean
rm -rf build sysroot iso kernel.iso
